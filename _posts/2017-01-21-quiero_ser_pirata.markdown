---
title: "¡Quiero ser pirata!"
author: Partido Interdimensional Pirata
layout: post
cover: "assets/covers/single/quiero_ser_pirata.png"
slider: "assets/covers/slider/quiero_ser_pirata.png"
signature: 0
toc: true
---

![](assets/images/quiero_ser_pirata.png)

Quiero ser pirata
=================

## Manifiesto Poético

Contra todo intento de estigmatizacion de nuestro nombre, reivindicamos a la
Piratería y sus luchas.

Si en aquel mundo dominado por monopolios depredadores, por la esclavitud, la
autocracia y el racismo, las Piratas se dieron a sí mismas igualdad, libertad,
autodeterminación y auténtica convivencia multicultural, ignorando toda
corrección política de su tiempo, entonces es que reivindicamos su bandera, su
legado y su nombre.

Porque nosotras crecimos en la periferia de lo que algunos llaman ilegalidad,
navegando caóticos mares culturales, habitando unas pocas y remotas islas
liberadas y no conocemos otra forma de organizarnos que no sea bajo un
principio de convivencia igualitaria: las Redes de Pares "P2P".

Resistimos entonces los monopolios, del conocimiento, la cultura y el
patentamiento de la vida, así como nos resistimos a las metrópolis del presente
que depredan los "recursos naturales" en sus colonias: el planeta entero.

Y como buenas Piratas y Filibusteras nos parecemos en algo más a nuestras
predecesoras: asaltamos naves, las del egoísmo y el saqueo, para quebrar la
opresión de las falsas leyes de la escasez, del copyright y sus artificiales
feudos inmateriales, fraudulentos espejismos propagandísticos que nos
restringen el acceso a este nuevo mundo de la abundancia, donde
todas podemos crear, copiar, multiplicar y compartir.

Como Piratas que somos creemos en la libertad, en la diversidad, en la
auto-organización, en la horizontalidad, en sentirnos pares e iguales, en
democratizar la creación, en la libertad de expresión y pensamiento; y como
nacimos compartiendo, creemos en compartir la mayor riqueza que atesoramos como
humanidad: la cultura, las ideas y el conocimiento, que liberadas del lastre
analógico y anacrónico de la escasez, solo queda liberarles del pesado lastre
de la codicia.

Como Piratas que somos, no somos ingenuas y sabemos que las mismas armas
digitales que por una parte nos liberan, por la otra encarcelan, cercenan y
mutilan.  Por eso defendemos la privacidad, el anonimato, las redes libres y la
neutralidad de Internet, como herramientas de liberación que nos permitan
seguir sosteniendo nuestra autonomía, capacidad de organizarnos y resistirnos a
las leyes del Estado.

Como Piratas que somos aprendemos sobre la marcha, construímos sobre la marcha,
aprendemos de las históricas luchas por la democratización del conocimiento y
la libertad de expresión y aprendemos de las luchas por proteger nuestros
bienes comunes, los recursos naturales: mares, vientos, llanuras y montañas.
Aprendemos que la libertad, la solidaridad y el compromiso colectivo son el
único camino sustentable para seguir existiendo como seres humanas en este
planeta.

Defendemos los "derechos humanos" por sobre cualquier interés o lobby,
considerando a la igualdad, de derechos y de oportunidades, para y entre todas
las personas, como un punto necesario para el desarrollo de una sociedad justa
y libre.  Por esto mismo creemos en el reconocimiento y autodeterminación de las
diferentes identidades culturales, de género y de cualquier otro tipo.

En un mundo donde la educación muchas veces es apenas formación para la
inserción en el mercado laboral, impulsamos la formación integral de las
personas a lo largo de toda su vida, la autoformación, el Libre Acceso a y
ejercicio de la Información, Conocimiento, Cultura y Educación.

Velamos por la participación social desde abajo y no a través de
representantes, intermediarios o Líderes Políticos.  En un desarrollo económico
responsable y sustentable, entendiendo que el paradigma extractivista destruye
los bienes comunes, condenando a las generaciones futuras.  En la organización
horizontal y el consenso.  En la resolución pacifica de los conflictos y en la
acción no violenta.

## Cómo acercarse

> ¡Mi nombre es Guybrush Threepwood y quiero ser pirata!

El paso mas rápido y fácil es entrar a la sala de [chat](#chat),
saludar, esperar un rato y conversar con nosotras.  También te podés
acercar a una reunión [Grog&Tor](#Grog&Tor).

## Acciones permanentes

### Grog&Tor

Son reuniones de autoformación en seguridad, vigilancia y
ciberactivismo.  Si es la primera vez que te pones en contacto con
nosotras, te recomendamos venir!

* <https://wiki.partidopirata.com.ar/GrogTor>

### Yo los paro

Es una plataforma de campañas donde las personas pueden contactar a las
legisladoras de un distrito para "aconsejarles" respecto a un proyecto o
problemática.

### Hackatones

Son reuniones para trabajar en proyectos internos.

### AFK Pirata

Los _Away From Keyboard_ son encuentros de camaradería pirata donde nos
juntamos a tomar cerveza, comer algo y charlar de lo que se nos ocurra.
Organizá el próximo!

* <https://wiki.partidopirata.com.ar/Como_organizar_un_encuentro_pirata>

### Asamblea anual

Es "la" reunión.  En ella hacemos un balance del tiempo pasado y las
acciones concretadas.  Nos sirve para reunir fuerzas en la política que
queremos seguir adelante, lo que queremos mejorar de ella o lo que ya
no queremos continuar.

### Utopía pirata

En Utopías piratas, seleccionamos textos que nos resulten interesantes
como bases políticas comunes.  La tarea es encontrar esos textos,
debatirlos, traducirlos si es necesario, editarlos, imprimirlos y
distribuirlos.

* <https://utopia.partidopirata.com.ar>

## Organización

### Cómo se toman decisiones

Las piratas tomamos decisiones por **consenso**.  Esto quiere decir que
presentamos nuestras propuestas, las discutimos entre todas y llegamos a
una posición común.  Esto no quiere decir que todas tenemos que votar de
la misma forma, sino que al tomar una decisión estamos teniendo en
cuenta las posiciones de todas las piratas.  El consenso es un proceso
que dura toda la vida de una acción, lo que nos permite actuar con
flexibilidad, darnos cuentas de nuestros errores e incorporar las
posiciones opuestas.

* Carta Orgánica:
* <https://utopia.partidopirata.com.ar/carta_organica_pirata.html>
* Herramientas para la democracia directa:
* <https://utopia.partidopirata.com.ar/democracia_directa.html>

### Cómo organizamos acciones

No tenemos representantes ni líderes.  Si hay que hacer algo, sos
bienvenida a sumarte en la medida de tus posibilidades.  Si no podés
cumplir con un compromiso que asumiste, avisá con tiempo y en lo posible
encontrá alguien que te reemplace.  Estos principios básicos son la
**adhocracia**.

### Regla de las tres piratas

La _regla de las tres piratas_ es una medida "a ojo".  Si tenés una
idea, a otras dos piratas les parece una buena idea y nadie se opone,
quiere decir que te podés poner manos a la obra.  Al menos a proponiendo
tu idea de forma más amplia en [Loomio](#loomio).

Acordate de esta canción:

> Si tu tienes muchas ganas de... 
> Si tu tienes la razón 
> y no hay oposición 
> no te quedas con las ganas de...

### Propuesta significa acción

Cualquier persona está invitada a sumarse, proponer cosas y realizarlas.
Proponer cosas para que las haga otra persona no lleva a ningún lado.
La mayor parte del tiempo vas a ver que nadie hace lo que a otra pirata
le pareció que estaría bueno hacer.  _Si ves que algo está sucio, agarrá
la escoba y ponete a limpiar_.

### Ni una pirata sola

No queremos que ninguna pirata se quede sola haciendo cosas, por lo
tanto: no dejes que ninguna pirata se quede sola y si te quedaste sola,
tirá bronca o dejá de lado la tarea hasta que estés acompañada.

### Código de camaradería

El único capital que tenemos como piratas son los piratas, sus
conocimientos y sus capacidades.  Preservemoslas manteniendo una buena
relación entre todas.

1. **Todas las piratas son buenas**

Para toda discusión siempre se supone que los demás actúan de buena fé y
que participa por verdadero interés pirata.  Nadie es un infiltrado de la
SIDE, la CIA o los reptilianos iluminati.

2. **Argumentos _ad hominem_**

Evitemos las descalificaciones personales, se debaten las ideas no las
personas.

3. **Que no se vaya Jose María**

El intercambio por texto es ineficiente.  Muchas veces no nos conocemos,
el sentido, la entonación y demás sutilezas de la comunicación pueden
perderse entre que escribimos y lo lee la otra pirata.

## Herramientas

### Cuenta Pirata

Tenemos una página web en un servidor.  En él, además tenemos acceso a
distintos servicios.  Algunos de ellos son los que mencionados en
[Herramientas](#Herramientas).  Con la cuenta pirata tenes acceso a todos
ellos.  Además tenemos _CalDAV_ para sincronizar piratas con todos los
eventos y actividades!

* <https://consenso.partidopirata.com.ar/d/EVaOzJnI/c-mo-creamos-las-cuentas-piratas>

* <https://tortuga.partidopirata.com.ar/remote.php/dav/calendars/fauno/eventos-pblicos/>

* <https://tortuga.partidopirata.com.ar/remote.php/dav/calendars/fauno/eventos-privados/>


### Loomio

Consenso es nuestra instancia de Loomio, una plataforma de
participación, debate y toma de decisiones.  La probamos y nos gustó, en
ella tomamos las decisiones.  Tené en cuenta que las decisiones no son
por voto y mayoría, sino por **consenso**.

* <https://wiki.partidopirata.com.ar/Consenso_(loomio)>

### Chat

La sala de chat se accede vía IRC o Telegram (son la misma).
Por favor recordá y tené presente nuestro **[Código de camaradería](https://wiki.partidopirata.com.ar/ConvivenciaPpar)**.

* Si querés entrar por Telegram, este link funciona actualmente:
* <https://t.me/joinchat/CH3fIkLegzTNPk-REpgbvQ>
* Podría ser que revoquemos ese enlace por algún motivo y no lo actualicemos aquí, avisanos!

* Si no tenés Telegram ni un cliente de IRC, podés usar este link:
* <https://webchat.pirateirc.net/#ppar>

* Si tenés un cliente IRC, los datos son estos:

> Servidor: irc.pirateirc.net
> Puerto: 6697
> SSL/TLS: Habilitado

* Si querés configurar el IRC en Pidgin:
* <https://wiki.partidopirata.com.ar/Pidgin_con_IRC>

### Redes sociales

No nos gustan las plataformas de redes sociales privativas.  Tenemos
serias discusiones con el uso de Facebook, Twitter y demás, aunque
mantengamos perfiles en algunas de esas plataformas, las tomamos como
herramientas de difusión, no como herramientas de organización.  Seguinos en:

* Diáspora\*: 
* [partidopirataargentina@diaspora.com.ar](https://diaspora.com.ar/u/partidopirataargentina)
* Mastodon: 
* [@pip@todon.nl](https://todon.nl/@pip)

### Listas de correo

Las listas de correo fueron nuestra primera herramienta de comunicación
y organización, que fuimos abandonando en favor de herramientas más
adecuadas para cada cosa, como IRC para el [chat](#chat) o
[Loomio](#loomio) para tomar decisiones.

### Pads

Los pads son textos colaborativos que nos permiten escribir textos con
una posición en común, ya que todas podemos producir el texto a la vez.
Cada participante tiene un color y mientras más colores haya quiere
decir que un texto es más rico.  No borres los colores!

* <https://pad.partidopirata.com.ar/>

## Auto-formación

### Piratas sombra

Las piratas sombras son piratas que se ofrecen voluntariamente a ayudar
a una pirata nueva a participar del partido.  Si querés participar y
necesitás una mano para ubicarte, pedí tus sombras P)

* <https://consenso.partidopirata.com.ar/d/r2wLy1Gz/-bienvenidxs-piratas->

### Utopías piratas

Las utopías piratas son textos que nos interesa difundir como material
de autoformación pirata en distintos temas.  Por ejemplo, _Herramientas
para la democracia directa_ son textos introductorios a la toma de
decisiones por consenso, la forma en que tomamos decisiones.

* <https://utopia.partidopirata.com.ar>


### Carta Orgánica

La Carta Orgánica es un documento de organización pirata que sobrevivió
nuestras ganas de ser un partido político reconocido por el Estado.  La
idea fue tener un documento organizativo que se adecue lo más posible a
nuestra forma real de organizarnos (frente a, por ejemplo, aceptar un
estatuto modelo pero verticalista y trabajar de "otra forma" en una red
de confianza).

### Wiki

En la wiki pirata documentamos algunos procesos, guías y etc.

* <https://wiki.partidopirata.com.ar>


### Zines / Stickers / Material gráfico

Los zines son hojitas de difusión rápida de ideas, creamos material
sobre cómo y por qué usar algunas de las herramientas afines a nuestra
liberación.

* <https://zines.partidopirata.com.ar>

### Grog&Tor

Si tenés ganas de conocernos y/o aprender sobre herramientas de seguridad y
privacidad, adoptar GNU en tu corazón y computadora, acercate a la próxima.
Sucede los segundos sábados de cada mes en el rango de 15 a 20hs.  El lugar es
rotativo, estate atenta en las [redes sociales](#redes-sociales), subscribite a
los [correos de difusión](mailto:difusion-subscribe@asambleas.partidopirata.com.ar)
o preguntá en el [chat](#chat)!

---

> "En la transmisión antigua el límite era la cantidad de espacio de las ondas
> radiofónicas.  Solo se podían transmitir 10 canales de televisión, e inclusive
> con el cable, tenías 500 canales.  En Internet todos pueden tener un canal.
> (...) Todos tienen una forma de expresarse.  La cuestión no es quién tiene
> acceso a las ondas radiofónicas, sino **quién controla la forma de encontrar
> personas.** Empiezas a ver que el poder se centra en sitios como Google [o
> Facebook], estos guardianes que te dicen adónde debes ir en
> Internet.  La gente que te brinda las fuentes de noticias e información.  No es
> que cierta gente tiene licencia para hablar, ahora todos pueden hablar.  La
> cuestión es quién se hace escuchar" ([Aaron
> Swartz](https://archive.org/details/TheInternetsOwnBoyTheStoryOfAaronSwartz),
> 8/Noviembre/1986 – 11/Enero/2013).

