---
title: El fenómeno de los trabajos de mierda
author: David Graeber
layout: post
signature: 0
---

# El fenómeno de los trabajos de mierda

> Publicado en _Guerrilla Translation_ como ["El fenómeno de los curros
> inútiles"](http://www.guerrillatranslation.es/2013/09/24/el-fenomeno-de-los-curros-inutiles/)

En el año 1930, John Maynard Keynes pronosticó que llegados a fin de
siglo, la tecnología habría avanzado lo suficiente para que países como
Gran Bretaña o Estados Unidos pudieran implementar una semana laboral de
15 horas.  No faltan motivos para creer que tenía razón, dado que nuestra
tecnología actual nos lo permitiría.  Y sin embargo, no ha ocurrido.  De
hecho, la tecnología se ha encauzado, en todo caso, para inventar las
condiciones para que todas trabajemos más.  Para lograrlo se han creado
trabajos que, en efecto, no tienen ningún sentido.  Enormes cantidades de
personas, especialmente en Europa y Estados Unidos, se pasan la
totalidad de su vida laboral realizando tareas que, en el fondo,
consideran totalmente innecesarias.  Es una situación que provoca una
herida moral y espiritual muy profunda.  Es una cicatriz que marca
nuestra alma colectiva.  Pero casi nadie habla de ello.

¿Por qué no se materializó nunca la utopía prometida por Keynes --una
utopía que se seguía anhelando en los sesenta?  La explicación más
extendida hoy en día es que no supo predecir el aumento masivo del
consumismo.  Ante la disyuntiva de menos horas o más juguetes y placeres,
elegimos colectivamente lo segundo.  Nos presentan una fábula muy bonita
pero, con solo reflexionar un momento, veremos que no puede ser cierto.
Indudablemente, presenciamos la creación de un sinfín de nuevos trabajos
e industrias desde los años 20, pero muy pocas de ellas tienen que ver
con la producción y distribución de sushi, de iPhones o de calzado
deportivo de moda.

Entonces, ¿cuáles son exactamente estos nuevos trabajos?  Un informe en
el que se compara el desempleo de EE.UU. entre 1910 y el 2000 nos da una
imagen muy clara (que, recalco, se ve prácticamente reflejada con
exactitud en el Reino Unido).  Durante el último siglo, ha disminuido
drásticamente la cantidad de trabajadoras empleadas en el servicio
doméstico, la industria y el sector agrario.  Simultáneamente, "los
puestos profesionales, directivos, administrativos, en ventas y en el
sector de servicios" se han triplicado, creciendo "de una cuarta parte a
tres cuartas partes de la totalidad de la fuerza laboral".  Es decir, tal
y como estaba previsto, muchos trabajos productivos se automatizaron
(aunque se tome en cuenta la totalidad de trabajadoras industriales del
mundo, incluyendo la gran masa de trabajadoras explotadas de India y
China, estas trabajadoras ya no representan un porcentaje de la
población mundial tan elevado como antaño).

Pero en vez de permitir una reducción masiva del horario laboral de modo
que todo el mundo tenga tiempo libre para centrarse en sus propios
proyectos, placeres, visiones e ideas, presenciamos una dilatación, no
tanto del "sector de servicios" como del sector administrativo.  Esto
incluye la creación de nuevas industrias, como son los servicios
financieros o el telemarketing y la expansión de sectores como el
derecho corporativo, la administración de la enseñanza y de la sanidad,
los recursos humanos y las relaciones públicas.  Estas cifras ni siquiera
reflejan a todas las personas que se dedican a proveer apoyo
administrativo, técnico o de seguridad para esas industrias, por no
mencionar toda la gama de sectores secundarios (cuidadoras de perros,
repartidoras de pizza 24hs.) que tan solo deben su existencia a que el
resto de la población pase tantísimo tiempo trabajando en otros
sectores.

Estos trabajos son lo que propongo denominar "laburos de mierda".

Es como si alguien estuviera inventando trabajos sin sentido solo para
tenernos a todas ocupadas.  Y acá precisamente es donde reside el
misterio.  Esto es exactamente lo que no debería ocurrir en el
capitalismo.  Es cierto que en los antiguos e ineficientes estados
socialistas como la Unión Soviética, donde el trabajo era considerado
tanto un derecho como una obligación sagrada, el sistema creaba todos
los trabajos que hicieran falta (éste es el motivo por el que en los
negocios soviéticos "se necesitaban" tres empleadas para vender un solo
churrasco).  Pero claro, se supone que este tipo de problemas se arregla
con la competitividad de los mercados.  Según la teoría económica
dominante, derrochar dinero en puestos de trabajo innecesarios es lo que
menos interesa a una compañía con ánimo de lucro.  Y aún así, no se sabe
muy bien por qué, pero ocurre.

Aunque muchas empresas se dediquen a recortar sus plantillas
despiadadamente, estos despidos y el correspondiente aumento de
responsabilidades para los que permanecen, invariablemente recaen sobre
quienes se dedican a fabricar, transportar, reparar y mantener las
cosas.  Debido a una extraña metamorfosis que nadie es capaz de explicar,
la cantidad de administrativas asalariadas parece seguir en expansión.
El resultado y esto pasaba también con las trabajadoras soviéticas, es
que cada vez hay más empleadas que teóricamente trabajan 40 o 50 horas
semanales pero que, en la práctica, solo trabajan esas 15 horas que
predijo Keynes porque pasan el resto de su jornada organizando o
atendiendo talleres motivacionales, actualizando sus perfiles de
Facebook o descargándose temporadas completas de series de televisión.

Evidentemente, la respuesta no es económica sino moral y política.  La
clase dirigente se ha dado cuenta de que una población productiva, feliz
y con abundante tiempo libre representa un peligro mortal (recordemos lo
que empezó a pasar la primera vez que hubo siquiera una aproximación a
algo así en los años sesenta).  Por otra parte, la noción de que el
trabajo es una virtud moral en sí mismo y que toda aquella que no esté
dispuesta a someterse a una disciplina laboral intensa durante la mayor
parte de su vida no merece nada, les resulta de lo más conveniente.

En cierta ocasión, al observar el aumento aparentemente ilimitado de las
responsabilidades administrativas en las instituciones académicas
británicas, me imaginé una posible visión del infierno.  El infierno es
un grupo de individuas que pasan la mayor parte de su tiempo
desempeñando tareas que ni les gustan, ni se les dan especialmente bien.
Imaginemos que se contrata a unas ebanistas altamente calificadas y que
éstas, de repente, descubren que su trabajo consistirá en pasarse gran
parte de la jornada friendo pescado.  Es más, se trata de un trabajo
innecesario --solo hay una cantidad muy limitada de pescados a freír.  Aun
así, todas se vuelven tan obsesivamente resentidas ante la sospecha de
que algunas de sus compañeras pasan más tiempo tallando madera que
cumpliendo con sus responsabilidades como freidoras de pescado, que
pronto nos encontramos con montañas de pescado mal cocinado desperdigado
por todo el taller y acaban dedicándose a eso exclusivamente.

Creo que es una descripción bastante acertada de la dinámica moral de
nuestra propia economía.

Soy consciente de que argumentos como éste se toparán con objeciones
inmediatas: "¿Quién sos vos para determinar qué trabajos son
‘necesarios’?  ¿Qué es necesario, a todo esto?  Sos profesor de
antropología, explícame qué necesidad hay de eso".  (De hecho, muchas
lectoras de prensa-basura valorarían mi trabajo como la definición por
excelencia de una inversión social desperdiciada).  Y en cierto sentido,
esto es indudablemente cierto.  No hay forma objetiva de medir el valor
social.

No me atrevería a decirle a una persona que está convencida de aportar
algo importante a la humanidad que, en realidad, está equivocada.  Pero,
¿qué pasa con quienes tienen la certeza de que sus trabajos no sirven de
nada?  Hace poco retomé el contacto con un amigo de la escuela que no
veía desde que teníamos 12 años.  Me quedé atónito al descubrir que,
primero, se había hecho poeta y, más adelante, fue vocalista en un grupo
de rock indie.  Incluso había escuchado algunos de sus temas en la radio
sin tener ni idea de que el cantante era mi amigo de la infancia.  No
cabe duda de que era una persona innovadora y genial y que su trabajo
había mejorado y alegrado la vida de muchas personas alrededor del
planeta.  Pero, tras un par de discos fracasados, perdió su contrato
discográfico y sumado a la presión de numerosas deudas y una hija recién
nacida, terminó, tal y como él lo describió, "eligiendo la opción que,
por defecto, eligen muchas personas sin rumbo: inscribirse en derecho".
Ahora es abogado mercantil para un prestigioso bufete neoyorquino.  Mi
amigo no titubeó en admitir que su trabajo carecía de valor alguno, que
no contribuía nada al mundo y que, según su criterio, ni siquiera
tendría que existir.

Llegados aquí, podemos plantearnos una serie de preguntas.  La primera
sería: ¿qué dice esto de nuestra sociedad, que parece generar una
demanda extremadamente reducida de poetas y músicas talentosas, pero una
demanda aparentemente infinita de especialistas en derecho empresarial?
(Respuesta: si un 1% de la población controla el grueso de las rentas
disponibles, el denominado "mercado" reflejará lo que ellos y nadie más
que ellos, perciben como útil o importante).  Es más, esto demuestra que
la gran mayoría de estas empleadas son conscientes de ello en realidad.
De hecho, creo que jamás he conocido a una abogada mercantil que no
pensara que su trabajo era una porquería.  Podríamos decir lo mismo de
casi todos los sectores nuevos mencionados anteriormente.  Existe toda
una clase de profesionales asalariadas que, al toparte con ellas en una
fiesta y confesarles que te dedicas a algo que podría considerarse
interesante (como, por ejemplo, la antropología) evitan hablar de su
profesión a toda costa.  Pero después de unas cuantas copas, te sueltan
toda una diatriba sobre la inutilidad y estupidez de su trabajo.

Aquí contemplamos una profunda violencia psicológica.  ¿Cómo vamos a
plantearnos una discusión seria sobre la dignidad laboral cuando hay
tanta gente que, en el fondo, cree que su trabajo ni siquiera debería
existir?  Inevitablemente, esto da lugar al resentimiento y a una rabia
muy profunda.  El peculiar ingenio de esta sociedad reside en el hecho de
que nuestros dirigentes han hallado la manera --como en el ejemplo de las
freidoras de pescado-- de que esa rabia se dirija precisamente en contra
de quienes desempeñan tareas provechosas.  Por ejemplo, parece que existe
una regla general que dictamina que, cuanto más claramente beneficioso
para las demás es un trabajo, peor se remunera.  De nuevo, es muy difícil
dar con una evaluación objetiva, pero una forma fácil de hacernos una
idea sería preguntando: ¿qué pasaría si todos estos sectores laborales
desaparecieran sin más?  Se diga lo que se diga de las enfermeras, las
basureras o las mecánicas, es evidente que si se esfumaran en una nube
de humo, los resultados serían inmediatos y catastróficos.  Un mundo sin
profesoras o trabajadoras portuarias no tardaría en estar en problemas,
e incluso un mundo sin escritoras de ciencia ficción o músicas de Ska
sería, sin duda, un mundo peor.  No está del todo claro cuánto sufriría
la humanidad si todos los inversores de capital privado, grupos de
presión parlamentaria, investigadores de relaciones públicas, actuarios,
vendedoras telefónicas, comisarios o asesores legales se esfumaran de
golpe.  (Hay quien sospecha que todo mejoraría notablemente).  No
obstante, exceptuando algunos ejemplos bastante desgastados, como el de
las médicas, dicha "regla" se cumple con sorprendente frecuencia.

Aún más perversa es la noción generalizada de que así es como deben ser
las cosas.  Este es uno de los secretos del éxito del populismo de
derecha.  Podemos comprobarlo cuando la prensa sensacionalista suscita el
recelo contra las trabajadoras del metro londinense por paralizar el
servicio durante una disputa contractual.  El solo hecho de que las
trabajadoras del metro puedan paralizar todo Londres demuestra la
necesidad de la labor que desempeñan, pero es precisamente esto lo que
parece incordiar tanto a la gente.  En Estados Unidos van aún más lejos;
los Republicanos han tenido mucho éxito propagando el resentimiento
hacia las profesoras o las obreras del sector automovilístico al llamar
la atención sobre sus salarios y prestaciones sociales supuestamente
excesivos (y no hacia los administradores de las escuelas o los
directivos de la industria automovilística, que son quienes causan los
problemas, lo cual sería más significativo).  Es como si les estuvieran
diciendo "¡Pero si tienen la suerte de enseñarle a las niñas!  ¡O de
fabricar coches!  ¡Hacen trabajos de verdad!  Y, por si fuera poco,
¡tienen la desfachatez de reclamar pensiones y atención sanitaria
equivalentes a las de la clase media!".

Si alguien hubiera diseñado un régimen laboral con el fin exclusivo de
mantener los privilegios del mundo de las finanzas, difícilmente podría
haberlo hecho mejor.  Las verdaderas trabajadoras productivas sufren una
explotación y una precariedad constantes.  El resto se reparte entre el
estrato aterrorizado y universalmente denigrado de las desempleadas y
esa otra capa más grande que básicamente recibe un salario a cambio de
no hacer nada en puestos diseñados para que se identifiquen con la
sensibilidad y la perspectiva de la clase dirigente (directivos,
administradores, etc.) --y en particular, de sus avatares financieros--
pero que, a la vez, fomentan el creciente resentimiento hacia cualquiera
que desempeñe un trabajo de indiscutible valor social.  Evidentemente,
este sistema no es fruto de un plan intencionado sino que emergió como
resultado de casi un siglo de ensayo y error.  Pero es la única
explicación posible de por qué, a pesar de nuestra capacidad
tecnológica, no se ha implantado la jornada laboral de tres o cuatro
horas.
