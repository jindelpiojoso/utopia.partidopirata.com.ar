---
title: "La revolución como problema técnico"
author: "Amador Fernández-Savater"
layout: post
signature: 0
---

# La revolución como problema técnico: de Curzio Malaparte al Comité Invisible

[Publicado originalmente en El
Diario](https://www.eldiario.es/interferencias/Curzio_Malaparte-Comite_Invisible_6_447315274.html),
octubre de 2015.

## No un teatro, sino una estructura

El escritor Curzio Malaparte es una referencia en el mundo de la
arquitectura por [la casa que él mismo
diseñó](https://www.youtube.com/watch?v=VqKbVfXBygM) (con Adalberto
Libera) y construyó en Capri.  Una especie de búnker de color rojizo
empotrado en una esquina rocosa de la isla napolitana, la Punta Masullo.
Casa Matta la llamaban, no en el sentido literal de Casa Loca, sino por
su parecido con los refugios militares que Malaparte había conocido
directamente durante su participación en la Primera Guerra Mundial.
Casamatas son los puntos de avanzada donde se instalan piezas de
artillería que martillean las posiciones enemigas con fuego de flanqueo.
Quizá por esa resonancia, a pesar de la belleza excepcional del
emplazamiento, Malaparte aseguraba vivir en una "casa triste, dura y
severa".  Como él mismo.

Al menos también en otro sentido, podemos considerar que Malaparte
habitaba efectivamente en una posición de vanguardia.  Nos referimos a su
teoría sobre el poder, desarrollada en un libro célebre durante la
primera mitad del siglo XX y hoy medio olvidado: _Técnica del golpe de
Estado_.  Un libro de espíritu maquiaveliano en el cual Malaparte se
propuso divulgar neutralmente, tanto a revolucionarias como a
conservadores, los saberes necesarios para ocupar (o defender) el poder
del Estado.  A partir de algunos ejemplos concretos, como la revolución
rusa o la marcha sobre Roma de Mussolini, Malaparte despliega una idea a
la vez sencilla y deslumbrante: el poder es logístico y reside en las
infraestructuras.  No es de naturaleza representativa y personal, sino
arquitectónica e impersonal.  No es un teatro, sino una estructura de
acero, un edificio de ladrillo, un canal, un puente, una central
eléctrica.  Conquistar el poder pasa, pues, por adueñarse, no tanto de la
organización política y burocrática de la sociedad, como de su
organización técnica.

El ejemplo más claro --y también más importante, en tanto que precursor
del resto-- es la particular historia malapartiana de la revolución
rusa.  En el corazón de este capítulo hay una discusión: entre Lenin y
Trotsky, entre el comité central del partido bolchevique y la jefatura
del Comité Militar Revolucionario.  Para Lenin y el partido bolchevique,
el proceso revolucionario consiste en suscitar y organizar un
levantamiento general de las masas proletarias que desemboque en el
asalto al Palacio de Invierno.  Para Trotsky y el Comité Militar
Revolucionario, la cuestión es de orden muy distinto.  La revolución no
pasa por combatir a pecho descubierto al gobierno y sus ametralladoras,
ni por tomar palacios o ministerios, sino por apoderarse silenciosa y
abruptamente de los órganos materiales de la máquina estatal: las
centrales eléctricas y telefónicas, las estaciones de ferrocarril, los
puentes, los puertos, los gasómetros, los acueductos, etc.  "Lenin es el
estratega, el ideólogo, el animador, el _deus ex machina_ de la
revolución; pero el creador de la técnica del golpe de Estado
bolchevique es Trotsky".

El problema de la insurrección es de orden técnico.  No se necesita la
participación masiva y heroica de miles de proletarias embravecidas,
sino formar e instruir a una tropa de asalto de obreras, soldadas y
marineras especializadas: mecánicas, electricistas, telegrafistas,
radiotelegrafistas, etc.  "Una pequeña tropa, fría y violenta, de mil
técnicas", dice Malaparte.  A las órdenes de un ingeniero-jefe con un
plan científico de la revolución: el mismo Trotsky.  El revolucionario
judío no se fía del ímpetu popular, no confía en la participación de las
masas.  Cree y apuesta a que se puede conquistar el Estado con un puñado
de personas: es cuestión de método, de técnica y de táctica, no de
circunstancias.  "La revolución no es un arte, sino una máquina; solo
técnicas pueden ponerla en marcha y solo otras técnicas pueden
detenerla", afirma.

Según la historia (¿o la fábula?) de Malaparte, las mil técnicas de
Trotsky se ejercitaron durante meses en "maniobras invisibles":
infiltrándose por todos lados, lograron documentar y mapear la
distribución y localización de los despachos, de las instalaciones de
luz eléctrica y teléfono, de los depósitos de carbón y de trigo, de las
estaciones de ferrocarril y los puentes, etc.  Llegado el momento,
burlaron la vigilancia policial de los _junkers_ de Kerenski (más
atentos a un posible levantamiento masivo y popular que al deslizamiento
de pequeños grupos) y tomaron todas las infraestructuras del Estado.
"Operar con poca gente en un terreno limitado, concentrar los esfuerzos
sobre los objetivos principales, golpear directa y duramente, sin ruido.
Una ofensiva simultánea, repentina y rápida, apenas dos o tres días de
lucha".

El asalto final al Palacio de Invierno fue espectacular y pasó a la
historia, pero en realidad fue simplemente la manera de comunicar al
mundo que el poder ya había cambiado de bando, haciendo caer a la vista
de todas una cáscara vacía.  Así se entiende la conocida sentencia de
Trotsky: la insurrección es "el puñetazo a un paralítico".

## Las técnicas son mundos

Los movimientos políticos de los últimos años, conocidos como
"movimientos de las plazas", son aparentemente más "leninistas" que
"trotskistas", hablando en un sentido malapartiano.  Las tunecinas que
detonaron la primavera árabe ocuparon la _Kasbah_, las griegas plantaron
sus tiendas de campaña frente al Parlamento en la plaza _Syntagma_, las
portuguesas intentaron entrar por la fuerza en la Asamblea de la
República, en España rodeamos el _Parlament_ catalán en junio de 2011 y
el Congreso el 25S de 2012...  Rodear, asaltar, ocupar los parlamentos:
los lugares de poder institucional han hechizado la atención y el deseo
de los movimientos de las plazas (y, tal vez por eso, los dispositivos
electorales-institucionales son ahora la continuación).  Pero, ¿se halla
el poder realmente ahí dentro, en el interior de esos edificios?

Un grupo anónimo retoma por su cuenta las preocupaciones de Malaparte y
abre una alternativa para el pensamiento y la acción.  Se llama Comité
Invisible y su primer libro, [La insurrección que
viene](https://translationcollective.files.wordpress.com/2010/09/la_insurrecccion-que-viene-def1-12.pdf),
editado en 2007, fue un paradójico _best-seller_ subversivo, traducido a
varias lenguas.  Ahora, el Comité Invisible publica un segundo libro
titulado [A nuestros
amigos](http://www.eldiario.es/interferencias/comite_invisible-revolucion_6_348975119.html),
escrito a muchas manos entre una constelación de colectivos y personas
implicadas activamente en experiencias de lucha y auto-organización.  Se
trata de un texto que replantea abiertamente la cuestión revolucionaria,
es decir, el problema de la transformación radical (de raíz) de lo
existente, pero decididamente por fuera de los esquemas del comunismo
autoritario que condujeron a los desastres del siglo XX.

En el capítulo dedicado a analizar la naturaleza del poder
contemporáneo, el Comité Invisible afirma que el gobierno ya no reside
en el gobierno (y que, por tanto, de poco vale sustituir a uno por
otro), sino que está más bien incorporado en los objetos que pueblan y
en las infraestructuras que organizan nuestra vida cotidiana (y de las
que dependemos completamente: pensemos en el agua, el gas, la
electricidad, el teléfono, Internet, etc.).  Toda Constitución (y, por
tanto, todo proceso constituyente) es papel mojado, porque la verdadera
Constitución es técnica, física, material.  Los "padres" de la
Constitución real (y no formal) no son profesores, políticos o juristas,
sino quienes diseñan, construyen, controlan y gestionan la
infraestructura técnica de la vida, las condiciones materiales de
existencia.  Por tanto, se trata de un poder silencioso, sin discurso,
sin explicaciones, sin representantes y sin tertulias en la tele; y al
cual es del todo inútil oponerle una contra-hegemonía discursiva.

Ignorar al poder político, centrarse en las infraestructuras: aquí
terminan las resonancias con el particular Trotsky de Malaparte.  Porque
para el Comité Invisible no se trata de "adueñarse" de la organización
técnica de la sociedad, como si ésta fuese neutra o buena en sí misma y
bastase simplemente con ponerla al servicio de otros objetivos.  De
hecho, precisamente ese fue el error catastrófico de la revolución rusa:
distinguir los medios y los fines, pensar por ejemplo que se podía
liberar el trabajo de la explotación y la alienación a través de las
mismas cadenas de montaje capitalistas.  No, los fines están inscritos en
los medios: una cadena de montaje vehicula cierto imaginario del trabajo
y la producción, no se puede poner simplemente "al servicio de" otras
finalidades.  Cada herramienta configura y a la vez encarna cierta
concepción de la vida, implica un mundo sensible.  _Google_, una
autopista o un supermercado son decisiones de mundo, civilizatorias.  No
se trata de "apoderarse" de las técnicas existentes, ni de conseguir que
funcionen más y mejor, como si el contexto social simplemente
"obstaculizase" el despliegue de sus potencialidades, sino de
subvertirlas, transformarlas, reapropiárlas... _hackearlas_.

## Un devenir-hacker colectivo

La _hacker_ es una figura clave en la propuesta política del Comité
Invisible.  La asociamos exclusivamente con el mundo de las redes
digitales o, aún peor, con el "terrorismo informático", pero no tiene
nada que ver.  Una hacker es cualquiera que tiene curiosidad por crear
algo nuevo o por resolver un problema, una apasionada del saber-hacer,
una _bricoleur_.  Podemos pensarlo también por fuera del mundo de los
_bytes_, en un sentido social más amplio, como todo aquel que se
pregunta (siempre mediante el hacer) cómo funciona esto, cómo se puede
interferir en su funcionamiento, cómo podría funcionar de otro modo.  Y
se preocupa por compartir sus saberes.

¿Por qué la hacker es una figura tan central en la propuesta política
del Comité Invisible?  Vivimos rodeadas cotidianamente de "cajas negras":
infraestucturas opacas que constriñen nuestras posibilidades y nuestros
gestos en un marco preestablecido.  Cuando encendemos un
electrodoméstico, cuando pagamos la factura del agua o la luz, cuando
compramos en un supermercado...  El capitalismo no triunfa a diario
porque tenga un discurso convincente, sino porque nos tiene atrapadas
materialmente en sus cajas negras.  El espíritu hacker rompe el hechizo
de un mundo naturalizado y normalizado, al que nos adaptamos como
podemos, revelando los funcionamientos, encontrando fallos, inventando
nuevos usos, etc.  "El código es la ley" dice una máxima central de la
filosofía hacker.  Es el código (técnico) y no la ley (política) quien
define la realidad: lo posible y lo imposible, las limitaciones y los
potenciales, etc.  Las hackers tocan el código, es decir, lo que hay
detrás de las superficies a la vista; cacharrean y alteran las técnicas
para ponerlas a su servicio.  Y esto no solo para ellas, sino para todas.

Pero no se trata de sustituir a las "mil técnicas" de Trotsky por "mil
hackers".  Seguiríamos teniendo ahí una casta especializada, un saber
separado y, por tanto, un poder autonomizado de la colectividad.  Lo que
se precisa más bien (y a lo que se parece un proceso revolucionario
efectivo) es un devenir-hacker colectivo, de masas, sin ingeniero-jefe.
Es decir, la puesta en común de saberes que no son opiniones sobre el
mundo, sino posibilidades muy concretas de hacerlo y deshacerlo.  Saberes
que son poderes.  Poder de construir y de interrumpir, poder de crear y
de sabotear.  Un devenir-hacker colectivo son miles de personas que
bloquean en tal punto neurálgico un mega-proyecto de infraestructuras
que amenaza con devastar un territorio y sus formas de vida.  Un
devenir-hacker de masas son miles de personas que construyen pequeñas
ciudades, capaces de reproducir la vida entera (alimentación, cuidado,
estudio, comunicación, sueño, etc.) durante semanas, en el corazón mismo
de las grandes.

Esto es lo que ocurrió en mayo de 2011 en la Puerta del Sol y en tantas
otras plazas de las ciudades españolas.  El engarce de mil
saberes-poderes distintos para construir otro mundo dentro de este
mundo.  La auto-organización de la vida en común, sin centro ni
ingeniero-jefe, sino a partir de las necesidades inmediatas que surgían,
coordinando descentralizadamente los esfuerzos, pensando mientras se
hacía, lo que se hacía y desde lo que se hacía.  Politizando todo lo que
el paradigma clásico de la política deja en la sombra: la materialidad
de la vida, aquello que designamos, desvinculándolo de lo político, como
lo "reproductivo", lo "doméstico", lo "económico", la "supervivencia" o
la "vida cotidiana" y que queda siempre fuera del espacio público.

Si el poder es "infraestructural", se trata entonces de hackear las
infraestructuras existentes y/o de construir nuevas, articuladas con
otros prácticas vitales y otros mundos en marcha.  Una socialización de
saberes que no toma necesariamente la forma de un "todas expertas en
todo" (algo imposible y no seguramente deseable), sino más bien de
alianzas, contaminaciones y conexiones.  Las "maniobras invisibles"
donde hoy se preparan los procesos revolucionarios son todos los
espacios donde se comparten riquezas, medios y saberes, los _hacklabs_,
los centros sociales, las escuelas de conocimientos comunes y de
contra-habilidades, los lugares de cacharreo, todos los puntos de cruce
entre técnicas y formas de vida disidentes.  ¡Menos mítines y más
_hacklabs_!

Desde su puesto de avanzada en Punta Masullo, el vigía sonríe.

# Post

## Referencias utilizadas

* Técnica del golpe de Estado, Curzio Malaparte, Editorial Ulises, 1931
* Malaparte: vidas y leyendas, Maurizio Serra, Tusquets, 2012
* A nuestros amigos, Pepitas de Calabaza y Sur+ de México.

## Comentario

Este texto retoma y prolonga algunas notas sobre la naturaleza
"logística" del poder escritas por primera vez en una ["reseña" del
último libro del Comité
Invisible](http://www.eldiario.es/interferencias/comite_invisible-revolucion_6_348975119.html).

En su forma actual, el texto fue publicado por primera vez en el número
371 (mayo, 2015) de Arquitectura, la revista oficial del Colegio Oficial
de Arquitectos de Madrid (Madrid: ea!  Ediciones de arquitectura).

## Agradecimientos

Gracias a las amigas por los comentarios útiles para la escritura del
texto: Carolina, Pepe, Álvaro, Marc, Diego y Ema (en recuerdo de nuestro
frustrado intento por entrar en la casamata de Malaparte).
