---
title: "Licencias libres"
author: Partido Interdimensional Pirata
layout: post
signature: 0
---

Licencias Libres
================

Definición de una obra libre
----------------------------

Para más información: <https://freedomdefined.org/Definition/Es>

Una obra (una película, un documental, una novela, un poema, una
fotografía, un tema, una pintura...) se considera libre cuando permite a
todas las personas que no son las autoras ni las detentadoras de sus
derechos (en general, grandes conglomerados de empresas que venimos a
llamar las industrias culturales), no solo apreciarla tal cual está,
sino modificarla, adaptarla, distribuirla, traducirla, remixarla,
mezclarla con otras obras -también de caracter libre- para generar más
obras libres.

Para ser libres las obras tienen que cumplir cuatro requisitos u otorgar
cuatro libertades básicas:

* Poder usar, ejecutar o interpretar la obra con cualquier propósito, en
  público o en privado, sin excepciones.
* Poder estudiar la obra.
* Poder distribuir copias, sin restricciones ni límites comerciales.
* Poder hacer obras derivadas y distribuirlas

Estas condiciones quedan plasmadas en modelos llamados licencias, que
codifican legalmente nuestras intenciones y los derechos que ampliamos
al resto del mundo al liberar una obra.

Algunas licencias son consideradas _copyleft_ ("izquierda de copia", la
inversión de la "derecha de copia" o _copyright_) porque salvaguardan la
apropiación de las obras libres añadiendo una condición de reciprocidad:
las obras, modificadas o no, deben mantener los mismos términos de su
liberación.  En la práctica, mantener la misma licencia.  Esto produce
que haya cada vez más obras en el bien común y que se promueva una cultura
de libre cooperación.

Existen otras licencias que limitan o restringen estas libertades y que
por lo tanto no son consideradas propiamente libres.

## TL;DR sobre derechos de autora y licencias

Por más que no queramos, el derecho de autora siempre está.  Una vez que
una obra se fija en un medio físico (deja de estar en nuestras cabezas)
automáticamente tiene derechos de autora, la registremos o no.  El
registro es opcional, solo nos da la seguridad de tener una fecha para
dirimir un plagio y una forma de recaudar del estado.

Las licencias son contratos legales que se aplican sobre ese derecho de
autora.  No lo reemplazan, sino que lo ejercen de forma que el derecho de
autora ya no sea monopólico y pase a ser un bien común.  Como autoras
ejercemos el derecho monopólico de autora sobre nuestras obras para
liberarlas.  Por eso si alguien no cumple con los términos de la licencia,
la licencia deja de valer, pero siguen valiendo los derechos de autora.

Para elegir una licencia libre solo es necesario ponerla bien visible en
la publicación, ej: "Este zine se licencia bajo la CC-BY-SA" y el link
al texto legal o la versión "legible por humanas".  Con eso es
suficiente.

No todas las licencias Creative Commons son libres.  Los términos "No
comercial" (NC) y "Sin obras derivadas" (ND) hacen que las obras
circulen gratuitamente pero no son realmente libres porque tienen
restricciones a los usos que se les pueden dar.  Solo admiten la
distribución gratuita y/o sin modificaciones.

Guarda con el término "No comercial" porque es engañoso.  En el texto
legal no significa "sin fines de lucro" que es la razón por la que la
mayoría de la gente las elije, sino que legalmente impide cualquier
intercambio de dinero.  Si otras personas imprimieran el zine para
difundirlo, ni siquiera podrían cobrar por el costo de reproducción.
Es decir, si por ejemplo, un centro cultural quiere pasar una pelicula
licenciada como no comercial, no podría ni vender comida ni bebida para
sostener el espacio...

Los términos "sin derivadas" pueden ser tentadores para las personas que
piensan que liberar una obra habilita al resto del mundo a
tergiversarla.  Pero una obra derivada puede ser una re-edición, una
traducción, ilustrarla, adaptarla, grabarla, remixarla con otros
medios...

Recomendamos elegir la CC-BY-SA (Atribución-CompartirIgual).  Esto hace
que cualquier modificación tenga que tener la misma licencia, nadie
puede apropiarse de la obra y cualquier aporte que le hagan debe ser a
su vez liberado, con lo que la cantidad de bienes comunes disponibles
aumenta.

Las licencias se pueden elegir acá:
<https://creativecommons.org/choose/?lang=es_AR>

## Por qué no usar CC-NC

Adaptado de: <https://freedomdefined.org/Licenses/NC/Es>

Cuando elegimos una licencia NC, nuestro trabajo no va a ser compatible
con proyectos de contenido libre que tienen filosofías y prácticas
libertarias o menos restrictivas.

Según el principio del copyleft (o, en la terminología de Creative
Commons, "Compartir-Igual", "BY-SA"): se pueden hacer trabajos
derivados, pero tienen que ser licenciados bajo los mismos términos.  No
se puede hacer una obra derivada mezclando obras BY-SA con NC, ya que ya
se no puede aplicar la licencia copyleft al trabajo entero.  Siempre
gana la licencia más restrictiva.

Las licencias NC pueden tener un efecto perjudicial para proyectos
colectivos emergentes.  A veces en el contexto de dichos proyectos,
surjen iniciativas que representan una oportunidad para superar la falta
de financiamiento y la brecha digital.  La gente con acceso a materiales
con licencias libres pueden distribuirlos con un pequeño beneficio
usando medios más tradicionales como las fotocopias o la grabación de
CD.  Los centros culturales independientes pueden proyectar películas
libres y sostener el espacio vendiendo bebida y comida, pidiendo un
aporte o cobrando una entrada.  Las colectivas autogestivas pueden jugar
un papel claramente beneficioso en la expansión del conocimiento libre y
la cultura libre.  Las obras pueden ser traducidas para ser difundidas y
superar brechas de idioma o de accesibilidad.

Una licencia NC detiene en origen cualquier tentativa de añadir valor.
Pero hay una alternativa.  Las licencias copyleft de Creative Commons
requieren que todo trabajo derivado del suyo se haga disponible como
obras libres en su totalidad.  Cualquier empresa que intente explotarnos
tendrá que poner su «valor agregado» libremente disponible a todo el
mundo.  El "riesgo" de explotación capitalista disminuye, porque los
capitalistas no van a liberar las mercancías o si lo hicieran, serían
libremente re-apropiables por nosotras.  El ejemplo más común que
escuchamos es "¿Y qué pasa si Coca-Cola (o Monsanto o la Evil-Corp que
sea) quiere usar nuestras obras para publicitarse?" Desde la
perspectiva de las obras libres, _copyleft_, esas empresas deberían
liberar a su vez todas sus modificaciones.

Las licencias sin una cláusula de copyleft, como la CC-Atribución
(CC-BY) solamente garantizan que permanezca libre la parte de la obra
creada por quienes la liberan, es decir que permiten la apropiación
capitalista sin retribución.

Las licencias NC parecieran cumplir el mismo efecto de detener la
explotación, pero en verdad limitan más la capacidad de las economías
autogestivas y de pares para poder sostenerse que la capacidad de las
Evil-Corp de apropiarse de nuestras obras.  Al usar una licencia NC,
estamos coartando cualquier estrategia de sostenibilidad.

Una licencia anticapitalista
----------------------------

Las licencias Creative Commons son un modelo de artefacto legal que nos
permite distribuir gratuita y libremente bienes culturales.  Sin embargo,
tienen varias opciones y a veces no son tan claros los alcances de cada
una, habiendo incluso varios niveles de libertades que permitimos.

Una de estas licencias es la versión NoComercial, que es muy utilizada
por colectivos que no quieren que sus producciones culturales sean
explotadas comercialmente por las formas de mercantilización capitalista
que cuestionan.  Los alcances de la licencia NoComercial van más allá de
eso y resulta difícil determinar dónde está el fin de lucro, impidiendo
legalmente a otras colectivas por ejemplo, distribuir o reproducir
copias de textos, películas y audios, apenas por el costo de la
producción o por un extra para mantener espacios y proyectos
autogestivos.

En estos casos es cuando los deseos que ponemos sobre la licencia
NoComercial producen lo contrario de lo que esperamos.  En lugar de hacer
retroceder el fantasma del explotador capitalista, estamos impidiendo la
sustentabilidad económica de colectivas y espacios de los que queremos
ver más en el mundo.

Si bien puede argumentarse que en estos casos lo mejor es usar una
licencia copyleft como la CompartirIgual, Dmytri Kleiner, el autor del
Manifiesto Telecomunista, nos dice que necesitamos ir más allá e
implementar una licencia copyfarleft (ultra izquierda de copia, frente a
la izquierda de copia, el _copyleft_) que cumpla las dos funciones,
impedir la mercantilización capitalista de la cultura a la vez que
permita a las colectivas y otros espacios donde no existe la explotación,
sustentarse económicamente.

Para esto modificó la licencia NoComercial convirtiéndola en
NoCapitalista, donde la explotación comercial de la distribución y
reproducción de una obra están permitidos solo a espacios donde la
explotación capitalista no existe, es decir que las colectivas
autogestionadas, las cooperativas y las empresas bajo control obrero
tenemos una ventaja económica sobre el mercado capitalista a la hora de
mantener vivos nuestros proyectos.  Esto también es parte de lo que
desarrolla Dmytri Kleiner en el Manifiesto Telecomunista.

## El código fuente de las obras

El software libre está definido por las cuatro libertades, que son
condiciones necesarias para considerarlos libres.  Una de ellas va más
allá de las licencias en sí y es el acceso al código fuente.  Sin poder
leer el código en el que están escritos los programas, no podemos saber
cómo están hechos, estudiarlos ni modificarlos.  El software es libre si
tiene una licencia libre y además es posible leer su código fuente.  A
veces, dependiendo del lenguaje de programación en el que se escribe,
el programa mismo expone su código fuente, pero otras veces no y hay que
distribuirlo por separado.  En términos técnicos, es la diferencia entre
lenguajes interpretados y compilados.  Los interpretados pueden ser
leídos e interpretados tanto por personas como por computadoras,
mientras que los compilados se dividen en dos partes, una legible por
humanas, es decir el código fuente y otra legible por computadoras, es
decir el código _objeto_ o _binario_.  Un archivo `.exe` por ejemplo, es
un archivo binario, ilegible por humanas.

Sin embargo, en el caso de las obras culturales, muchas veces se
considera que son libres solo por tener una licencia libre, pero el
formato en el que se distribuyen no se presta para nada al estudio y
modificación.  Un libro liberado en formato PDF por ejemplo, es liberado
en el peor formato posible para su estudio y modificación, porque
equivale al código binario de un programa.  Podemos leerlo y apreciar su
diseño, pero si copiamos y pegamos el texto, perdemos el formato.
Tampoco podemos reproducir su diseño.  Para eso necesitamos los archivos
editables y estos no suelen ser liberados.  Otros formatos, como el ePub
o las páginas web, son un poco más amigables.

Esto impide, en el caso de los textos, que preparar su traducción
conlleve un trabajo de extracción del texto y recuperación del formato
(títulos, cursivas, negritas, notas al pie...) para poder empezar el
trabajo de traducción.

### El texto plano es el código fuente ideal

Así como en el desarrollo de software, que se escribe en archivos de
texto plano y después se convierte o no en código binario, proponemos
que el código fuente de los libros es el texto plano.  Un archivo que se
puede abrir en cualquier editor, leer, modificar e interpretar.  Luego
hay herramientas que hacen la maquetación en base a plantillas que
podemos editar a su vez como texto plano (o en editores gráficos que
interpretan estos archivos) y que al poner todo junto forman un libro.
Incluso, con las herramientas correctas, es posible generar varios
formatos de una sola vez a partir de un único archivo de texto.

En los casos en que no es posible trabajar en texto plano, proponemos
servirse de herramientas libres que usan formatos de archivo fuente
libres, de forma que tengamos la seguridad que el libro siempre pueda
ser modificado (incluso por nosotras mismas!) y no esté atado a
versiones específicas de algún software privativo y ayudarnos a prevenir
caer en la lógica de la obsolescencia programada donde siempre tenemos
que tener la última versión de todo.

Formatos:

* [markdown](https://es.wikipedia.org/wiki/Markdown)
* [svg](http://inkscape.org)
* [sla](https://www.scribus.net/)

Software:

* [markdown](https://es.wikipedia.org/wiki/Markdown) con nuestro editor
  de texto preferido!
* [pandoc](http://pandoc.org/), para convertir archivos markdown a
  diversos formatos (pdf, epub, etc.), usando plantillas
* [inkscape](https://inkscape.org/es/), para diseñar y editar tapas en
  svg
* [scribus](https://www.scribus.net/), para diseñar y maquetar
  publicaciones visualmente más complejas (revistas, fanzines, etc.)

### Las tipografías también tienen que ser libres

Las tipografías que usamos en nuestros textos, creáse o no, también
tienen _copyright_ / derechos de autora.  Pero podemos usar tipografías
libres, que también son publicadas con licencias que nos permiten
usarlas en nuestras publicaciones libres.

Existen licencias libres para fuentes, por ejemplo la _SIL Font
License_.

* [The League of Movable Type [La liga de los tipos móviles]](https://www.theleagueofmoveabletype.com/)
* [Font Library [Biblioteca de tipografías]](https://fontlibrary.org/)

¡La copia comparte cultura!
---------------------------

Desde el PIP, sostenemos que _copiar no es robar_.  Cuando se hace una
copia, nadie pierde y todas ganan.  Promovemos la copia como forma de
compartir cultura y conocimiento.  Por eso, frente a esos horribles
logos que arruinan los libros diciendo y amenazando que "la fotocopia
mata el libro y es un delito", nosotras decimos que _la copia comparte
cultura_.  Invitamos a todas las personas que comparten sus obras
preferidas a cambiar ese tipo de imágenes por una más adecuada como la
que ves en la contraportada y que podés descargar de acá:

<https://utopia.partidopirata.com.ar/assets/images/la_copia_comparte_cultura.png>

![](/assets/covers/contra_licencias_libres.png)
