---
title: "#Apostatemos de las redes sociales de vigilancia masiva"
author: Partido Interdimensional Pirata
layout: post
cover: assets/covers/apostatemos_de_las_redes_asociales.png
---

> **Apostatar** es el acto de renunciar a una creencia (religiosa,
> política, entre otras).  La apostasía colectiva[^apostasia]

[^apostasia]: Más información sobre Apostasía Colectiva:
  <http://apostasia.com.ar/>

Esto no quiere decir que abandonemos formas de encontrarnos,
informarnos, compartir y tener discusiones interesantes. Exis-
ten redes sociales libres y comunitarias.
Para eso, planteamos distintas estrategias y escenarios. Si te
estabas planteando dejar de usar Facebook (y Twitter y Google
y etc.), seguí leyendo :)
# Drástico
Si estás podrida y no querés saber más nada, podés cerrar tus
cuentas y venirte al Fediverso.
El Fediverso es una red de nodos de comunicación que se inter-
conectan entre sí, se federan. Si para comunicarte con alguien
en Facebook había que tener también una cuenta en Facebook
y lo mismo para Twitter, y así para todas las plataformas, en el
Fediverso sólo es necesario elegir un nodo, el que nos caiga más
simpático, el que nos parezca más confiable y a través de este, ya
nos podemos comunicar con el resto.
Para esto existen redes como Diáspora, GNU Social, Mastodon,
Friendica, Hubzilla, entre tantos otros, que son software que
permite que cualquier comunidad maneje su propio nodo. Si
antes esta capacidad solo la tenían empresas, ahora cualquier
colectivo puede armar su red y federarla con las demás.


# Organización
Hay colectivas que están usando Facebook no solo para comu-
nicar sus actividades, sino para organizarse y tomar decisiones
con sus compañeras. Pero como dice Audre Lorde, “¡las herra-
mientas del amo nunca desmantelarán la casa del amo!”.
Esto quiere decir que si bien Facebook tiene grupos, dentro de
su modelo de sociedad no existe la organización horizontal.
Para Facebook, todas somos individuas vociferando nuestra
opinión.
Existen otras herramientas diseñadas específicamente bajo un
modelo de organización horizontal y de toma de decisiones por
consenso, como Loomio. Loomio es desarrollado a partir de la
experiencia de Occupy y publicado como software libre. Dentro
del Partido Interdimensional Pirata tenemos nuestra propia
instancia e invitamos a cualquier colectiva interesada a usarlo.


# Ayuda mutua
Como la apostasía debe ser colectiva, nos proponemos juntar-
nos y ayudarnos entre todas a abandonar las redes capitalistas,
migrar hacia las redes libres y comunitarias y seguir encontrán-
donos en el ciberespacio, sin explotación, sin manipulación, sin
acoso.
¡Vení con nosotras a apostatar!

# Disclaimer
Existen alternativas distribuidas, anónimas, cifradas.
Las "apps" y nodos que podamos recomendar aquí son sólo algunas,
elegidas con criterios de seguridad y libertad para ciudadanas.
Sin embargo, un medio de comunicación libertario para la
organización de actividades que el estado y las instituciones reprimen
sistemáticamente (ley anti terroristas. cof. cof!), solo es posible con
un
poco mas de esfuerzo. ¿Crees que tu grupa, colectiva, partido o
movimiento querrá mejorar su uso de tecnologías de información y
comunicación?
Suscríbanse a nuestra lista de difusión para conocernos en
algún evento, o acerquen sus dudas a nuestra dirección de contacto
...no existe la magia interdimensional pero hay mucho que aprender,
enseñar y, por qué no, desarrollar.
PARTIDO
INTERDIMENSIONAL
PIRATA
Mail: contacto@par�dopirata.com.ar
Lista: difusion-subscribe@asambleas.par�dopirata.com.ar
Diáspora*: par�dopirataargen�na@diaspora.com.ar
Mastodon: @pip@todon.nl
Nosotras también estamos en proceso de apostatar P)
Twi�er: @Par�doPirataAr
Facebook: Par�do Pirata Argen�no
